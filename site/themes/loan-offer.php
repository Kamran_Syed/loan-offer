<?php
/**
 * Template Name: Loan Offer
 
 */

get_header(); ?>
	<div style="margin:0 1em;float:left;clear:left;">
		<div id="single-page-heading" class="new-container" role="main">
			<div style="font-size:3em;line-height: 50px;color: #000000;">
				Get Approved for Cash in Minutes.Up to $10,000 Standing By.
			</div>
			<hr>
			<div style="font-size:2em;line-height: 50px;color: #000000;">
				Apply now to get matched with a Lender and get cash for: 
			</div>
			<hr>
			<div style="float:left;clear:left;">
				
				<div style="float:left;">
					<div style="">
						<img src="<?php echo trailingslashit(  get_template_directory_uri() ); ?>images/load_offer.png"></a>
					</div>
				</div>
				<div style="float:left;margin-left:14em;">
					<img src="<?php echo trailingslashit(  get_template_directory_uri() ); ?>images/fix_your_credit.jpg"></a>
				</div>
			<div style="float:left;clear:left;font-size:2.5em;line-height: 50px;color: #000000;">
				<div style="font-size:0.9em;margin-top:1em;color: #000000;">
						How much do you need?
					</div>
					<div id="LFInlineFormContainer" class="LFInlineFormContainer LFInlineForm">
						<script type="text/javascript" >
							var LFInlineForm = {};
							var head_conf = { head: "LFHead" };
								   
							if (typeof jQuery != 'undefined') {
								LFInlineForm.OldjQuery = jQuery;
							}
						</script>
						<script type="text/javascript" src="https://appcompleted.com/Content/Global/Scripts/ThirdParty/head.js"></script>
						<script type="text/javascript" src="https://appcompleted.com/InlineForms/Loader.ashx?aid=LF15307&ShortForm=1&MainFormURL=/INSERT-PAGE-NAME-HERE.html&InlineFormStyle=InstallmentLoan501"></script>
					</div>
					<div id="LFInlineFormContainer" class="LFInlineFormContainer LFInlineForm">
						<script type="text/javascript" >
							var LFInlineForm = {};
							var head_conf = { head: "LFHead" };
								   
							if (typeof jQuery != 'undefined') {
								LFInlineForm.OldjQuery = jQuery;
							}
						</script>
						<script type="text/javascript" src="https://appcompleted.com/Content/Global/Scripts/ThirdParty/head.js"></script>
						<script type="text/javascript" src="https://appcompleted.com/InlineForms/Loader.ashx?aid=LF15307&InlineFormStyle=InstallmentLoan501"></script>
					</div>
			</div>
			<div style="float:left;clear:left;font-size:2.5em;line-height: 50px;color: #000000;">
				Take Advantage of What An Installment Loan Can Offer You 
			</div>
			<div style="font-family: cursive;line-height: 1.3em;float:left;clear:left;color: #000000;">
				<p>During these uncertain economic times, many people are finding themselves faced with a situation where they could use some financial assistance. Whether it be for an emergency, home improvement, consolidating debt or even a family vacation – a low interest personal or installment loan is a safe and reliable way to meet your financial needs. At CreditApplication.com, we specialize in quickly and efficiently matching people to the personal or installment loan that's right for them.</p>
				<p>Compared to other loan options, CreditApplication.com offers greater flexibility with how much a person can borrow. Utilizing our comprehensive network of lenders we can offer wide ranges of funding, and give you the opportunity to be qualified for as much as $10,000.</p>
			</div>
			<div style="margin-top:1em;float:left;clear:left;font-size:2.5em;line-height: 50px;color: #6E8DB5;">
				What Are Installment Loans? 
			</div>
			<div style=" font-family: cursive;line-height: 1.3em;float:left;clear:left;color: #000000;">
				<p>Broadly defined an installment loan is repaid over time with a set number of scheduled payments. It is also a form of loan that can be utilized for practically any purpose whatsoever. This can be beneficial in any scenario where additional funds are required.</p>
				<p>There are three types of personal loans available through our network: peer-to-peer loans, personal installment loans and bank personal loans. You can learn more about these products by viewing the How It Works page on our site.</p>
			</div>
			
			<div style="margin-top:1em;float:left;clear:left;font-size:2.5em;line-height: 50px;color: #6E8DB5;">
				Simple and Secure Online Loans
			</div>
			<div style="font-family: cursive;line-height: 1.3em;float:left;clear:left;color: #000000;">
				<p>We understand that searching for a loan can often be a daunting and confusing task, especially given the wide array of options that are available. We had this in mind when we created CreditApplication.com.</p>
				<p>Every aspect of our service has been designed and optimized with the customer in mind. We strive to offer you the most innovative and user friendly experience possible.</p>
				<p>Given that each individual lender is looking for a particular type of customer, many people end up getting turned down multiple times for any number of reasons – which can sometimes be as simple as living in the wrong state. What makes CreditApplication.com unique is that we have assembled the single largest network of responsible personal and installment loan lenders available anywhere.</p>
				<p>Our proprietary technology evaluates your application and matches it to the lender best suited for your specific situation. This means that by submitting a loan request on CreditApplication.com you have the highest chance of getting approved for the loan you desire.</p>
				<p>With our simple Two Step application, it has never been easier to find out if you qualify for the help you need, and enjoy financial security.</p>
				<p>To begin the process of getting your installment loan, please click here.</p>
			</div>
			
			<div style="margin-top:1em;float:left;clear:left;font-size:2.5em;line-height: 50px;color: #6E8DB5;">
				Important Information
			</div>
			<div style="margin-top:1em;float:left;clear:left;font-size:1.5em;line-height: 50px;color: #6E8DB5;">
				Important Points to Consider
			</div>
			<div style="font-family: cursive;line-height: 1.3em;float:left;clear:left;color: #000000;">
				<p>CreditApplication.com is only partnered with professional lenders who provide potential borrowers with sufficiently detailed information concerning loan terms and conditions prior to their accepting any personal loan offer. We recommend that you closely view the terms of any loan offer you get.</p>
			</div>
			<div style="margin-top:1em;float:left;clear:left;font-size:1.5em;line-height: 50px;color: #000000;">
				Disclaimer:
			</div>
			<div style="font-family: cursive;margin-bottom: 2em;line-height: 1.3em;float:left;clear:left;color: #000000;">
				<p>THE OPERATOR OF THIS WEB SITE IS NOT A LENDER, is not a loan broker, and does not make lending decisions on behalf of lenders. This Web Site does not constitute an offer or solicitation to lend. This site will submit the information you provide to a lender who make installment loans to borrowers who meet its lending criteria. Providing your information on this Web Site does not guarantee that you will be approved for a loan. The operator of this Web Site is not an agent, representative or broker of any lender and does not endorse any particular lender or charge you for any service or product. Not all lenders can provide the maximum amount advertised. Cash transfer times may vary between lenders and may depend on your individual financial institution. In some circumstances faxing may be required. This service is not available in all states, and the states serviced by this Web Site may change from time to time and without notice. For details, questions or concerns regarding your installment loan, please contact your lender directly. installment loans are meant to address immediate cash needs and are not a long term solution for financial problems. Residents of some states may not be eligible for a installment loans based upon lender requirements. </p>
			</div>
		</div>
	</div>
	
	
	
	
<footer class="wraper" style="padding: 1.714285714rem 0;  font-size: 0.857142857rem;
  line-height: 2;margin-right: auto;">
    <section class="head-els footer">
		<a href="#">About Us |</a>
		<a href="http://www.creditapplicationaff.com/terms-conditions/">Terms and Conditions |</a>
		<a href="http://www.creditapplicationaff.com/privacy-policy/">Privacy Policy |</a>
		<a href="#">Site Map |</a>
		<a href="#">Truth about lending |</a>
		<a href="#">Rates</a>
		</ul>
		<div>
			<p>Copyright © 2015 Credit Application.com CreditApplication.com recommends that you read the Terms and Conditions and Privacy Policy of this website. CreditApplication.com is a leading consumer finance company in the United States, offering a range of services: including personal loans, car loans, credit cards, credit repair and promotional retail finance.</p>
		</div>
    </section>
</footer>
<?php //get_footer(); ?>